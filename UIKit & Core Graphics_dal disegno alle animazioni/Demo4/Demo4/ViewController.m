//
//  ViewController.m
//  Demo4
//
//  Created by Diego Caridei on 19/06/14.
//  Copyright (c) 2014 com.diegocaridei. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    //Creiamo una ImageView via codice
    UIImageView *iv=[[UIImageView alloc]initWithFrame:CGRectMake(0, 20, 320, 300)];
    NSMutableArray *arrayImage=[[NSMutableArray alloc]init];
    //Carichiamo le immagini all'interno dell'array
    for (int i=1; i<=10; i++) {
        UIImage *image=[UIImage imageNamed:[NSString stringWithFormat:@"%d",i]];
        [arrayImage addObject:image];
    }
    //Con questo metodo impostiamo il numero di volte che l'animazione deve ripetersi
    //senza questo metodo l'animazione si ripeterà sempre
    iv.animationRepeatCount=3;
    iv.animationImages=arrayImage;
    //settiamo la durata dell'animazione
    iv.animationDuration=1;
    self.view.backgroundColor=[UIColor blackColor];
    [self.view addSubview:iv];
    //Avviamo l'animazione
    [iv startAnimating];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
