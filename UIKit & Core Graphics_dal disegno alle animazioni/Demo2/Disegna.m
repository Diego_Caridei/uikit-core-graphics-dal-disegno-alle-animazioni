//
//  Disegna.m
//  Demo2
//
//  Created by Diego Caridei on 18/06/14.
//  Copyright (c) 2014 com.diegocaridei. All rights reserved.
//

#import "Disegna.h"

@implementation Disegna

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    //bezierPathWithRect ci consente di creare un poligono
    UIBezierPath *path=[UIBezierPath bezierPathWithRect:CGRectMake(20, 20, 100, 100)];
    //impostiamo il colore
    [[UIColor yellowColor]setFill];//impostiamo il colore per l'interno del poligono
    [[UIColor brownColor]setStroke];//impostiamo il colore per il bordo del poligono
    path.lineWidth=5;//spessore del contorno
    //applichiamo il tutto
    [path fill];
    [path stroke];
    
    UIBezierPath *cerchio=[UIBezierPath bezierPathWithOvalInRect:CGRectMake(200, 270, 100, 100)];
    [[UIColor redColor]setFill];
    [[UIColor blueColor]setStroke];
    cerchio.lineWidth=5;
    [cerchio fill];
    [cerchio stroke];
    
    UIBezierPath *angArrot=[UIBezierPath bezierPathWithRoundedRect:CGRectMake(150, 150, 100, 100) cornerRadius:20];
    [[UIColor purpleColor]setFill];
    [[UIColor whiteColor]setStroke];
    angArrot.lineWidth=5;
    [angArrot fill];
    [angArrot stroke];
    
    
    
    
    
    
}

@end
