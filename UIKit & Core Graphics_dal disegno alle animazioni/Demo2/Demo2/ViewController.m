//
//  ViewController.m
//  Demo2
//
//  Created by Diego Caridei on 18/06/14.
//  Copyright (c) 2014 com.diegocaridei. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // creiamo un oggetto di segna lo allochiamo e lo inizializiamo con un frame della stessa grandezza dello schermo
    Disegna *disegna=[[Disegna alloc]initWithFrame:CGRectMake(0, 0, 320, 568)];
    [self.view addSubview:disegna];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
