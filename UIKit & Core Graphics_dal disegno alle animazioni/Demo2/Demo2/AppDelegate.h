//
//  AppDelegate.h
//  Demo2
//
//  Created by Diego Caridei on 18/06/14.
//  Copyright (c) 2014 com.diegocaridei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
