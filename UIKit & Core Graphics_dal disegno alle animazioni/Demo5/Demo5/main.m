//
//  main.m
//  Demo5
//
//  Created by Diego Caridei on 21/06/14.
//  Copyright (c) 2014 com.diegocaridei. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
