//
//  ViewController.m
//  Demo5
//
//  Created by Diego Caridei on 21/06/14.
//  Copyright (c) 2014 com.diegocaridei. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    //Dichiariamo un oggetto UIImageView e impostiamo un immagine
    UIImageView *imageView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"palla.png"]];
    // lo posizioniamo nel punto P(5,10) e impostiamo la sua grandezza 50X50
    imageView.frame=CGRectMake(5, 15, 50, 50);
    imageView.alpha=0;//in questo modo l'immagine sarà leggermente visibile
     //La classe UIView ha già dei metodi che consentono l'animazione
    //una di questi metodi è animateWithDuration
    
    [UIView animateWithDuration:2.0 delay:0 options:UIViewAnimationOptionAutoreverse animations:^{
        //la nostra animazione durerà 3 secondi
        //in questi 3 secondi la nostra immagine da trasparente diveterà nitida
        imageView.alpha=1;
        //Inoltre la nostra image view si sposterà dal punto P(5,15) al punto P(100,250)
        imageView.frame=CGRectMake(100, 250, 50, 50);
        imageView.transform=CGAffineTransformMakeRotation(M_PI);
    }completion:^(BOOL fine){
        imageView.alpha=0;
        
    }
     ];
    /*Innesto
    [UIView animateWithDuration:3.0 animations:^{
        //la nostra animazione durerà 3 secondi
        //in questi 3 secondi la nostra immagine da trasparente diveterà nitida
        imageView.alpha=1.0;
        //Inoltre la nostra image view si sposterà dal punto P(5,15) al punto P(100,250)
        imageView.frame=CGRectMake(100, 250, 50, 50);
    
    }
                     completion:^(BOOL fine){
                         [UIView animateWithDuration:1.0 animations:^{
                             
                         }];
                     }
     ];
     */
    
    [self.view addSubview:imageView];
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
