//
//  Disegna.m
//  Demo1
//
//  Created by Diego Caridei on 16/06/14.
//  Copyright (c) 2014 com.diegocaridei. All rights reserved.
//

#import "Disegna.h"

@implementation Disegna

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    //In questo modo abbiamo un referimento alla schermata corrente
    CGContextRef cotext =UIGraphicsGetCurrentContext();
    //disegnamo un rettangolo, per prima cosa settiamo il colore
    CGContextSetFillColorWithColor(cotext, [UIColor yellowColor].CGColor);
    //disegnamo il rettangolo, come parametri richiede il contesto e le coordinate x,y,larghezza,altezza
    CGContextFillRect(cotext, CGRectMake(40, 40, 100, 200));
    
    
}

@end
