//
//  main.m
//  Demo1
//
//  Created by Diego Caridei on 16/06/14.
//  Copyright (c) 2014 com.diegocaridei. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
