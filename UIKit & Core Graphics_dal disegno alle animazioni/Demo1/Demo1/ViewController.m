//
//  ViewController.m
//  Demo1
//
//  Created by Diego Caridei on 16/06/14.
//  Copyright (c) 2014 com.diegocaridei. All rights reserved.
//

#import "ViewController.h"
#import "Disegna.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    //Inizializziamo la nostra classe e impostiamo le coordinate
    Disegna *disegna=[[Disegna alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];
    //A questo punto non facciamo altro che dire alla nostra view
    // di aggiungere un'altra view al suo interno ovvero quella che abbiamo creato
    [self.view addSubview:disegna];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
